# Root project Yaroshenko.Tools

## Install project
* `git clone --recurse-submodules -j8 git@gitlab.com:ya_tools/root-project-yaroshenko.tools.git`
* read other readmes (`backend/readme.md`, `frontend/readme.md`)
## Run project
* `docker-compose -f docker-compose.prod.yml up`
